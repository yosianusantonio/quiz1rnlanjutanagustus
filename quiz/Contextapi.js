import React, { useState, createContext } from 'react'

export const RootContext = createContext();
import Jawab2 from './Jawaban2'

const ContextAPI = () => {

    const [ name, setName ] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider value={{
            name
        }}>
           <Jawab2/>
        </RootContext.Provider>
    )
}

export default ContextAPI;

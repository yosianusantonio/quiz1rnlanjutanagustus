import React, {useContext} from 'react'
import {View, Text, FlatList} from 'react-native'
import {RootContext} from '../quiz/Contextapi';

const Jawab2 = () => {
    const state= useContext(RootContext)
    console.log("Todolist -> state", state)
    const data2 = state.name


    const Task = ({item,index}) => (
        
            <View>
                <Text>{item.name}</Text>
                <Text>{item.position}</Text>
            </View>
    )


    return(
        <FlatList 
            data={state.name}
            renderItem={Task}
            />
    )
}

export default Jawab2